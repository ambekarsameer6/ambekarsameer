const progressBar = document.querySelector('.progressBar');
const section = document.querySelector('section');

const scrollProgressBar = () => {
	let scrollDistance = -(section.getBoundingClientRect().top);
	let progressPercentage =
		(scrollDistance /
			(section.getBoundingClientRect().height -
				document.documentElement.clientHeight)) * 100;

	let val = Math.floor(progressPercentage);
	progressBar.style.width = val + '%';

	if (val < 0) {
		progressBar.style.width = '0%';
	}
};

window.addEventListener('scroll', scrollProgressBar);

function toggle_vis(event, id) {
  event.preventDefault();
  var elem = document.getElementById(id);
  var link = document.getElementById('toggleLink');
  if (elem.style.display === 'none' || elem.style.display === '') {
    elem.style.display = 'block';
    link.textContent = 'Show Less';
  } else {
    elem.style.display = 'none';
    link.textContent = 'Show More';
  }
}

document.getElementById('moreNewsBtn').addEventListener('click', function() {
  const hiddenItems = document.querySelectorAll('.news-item.hidden');
  hiddenItems.forEach(function(item) {
    item.classList.remove('hidden');
  });
  this.style.display = 'none'; // Hide the button after clicking
});